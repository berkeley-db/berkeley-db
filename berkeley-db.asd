(defpackage berkeley-db-system
  (:use #:cl #:asdf))

(in-package :berkeley-db-system)

(eval-when (:load-toplevel :execute)
  (oos 'load-op '#:cffi-grovel))

(defsystem berkeley-db
  :description "A CFFI interface to Sleepycat's Berkeley DB."
  :version "0.1"
  :author "Greg Pfeil <greg@technomadic.org>"
  :licence "LLGPL"
  :depends-on (cffi cffi-unix)
  :components ((:module "src"
                        :components
                        ((:file "package")
                         (cffi-grovel-file "types" :depends-on ("package"))
                         (:file "berkeley-db" :depends-on ("package"))
                         (:file "support" :depends-on ("types" "berkeley-db"))
                         (:file "database" :depends-on ("support"))
                         (:file "cursor" :depends-on ("database"))
                         (:file "environment" :depends-on ("support"))
;;                          (:file "record" :depends-on ("support"))
;;                          (:file "locking" :depends-on ("environment"))
                          (:file "logging" :depends-on ("environment" "cursor"))
;;                          (:file "memory-pool" :depends-on ("environment"))
;;                          (:file "mutex" :depends-on ("environment"))
;;                          (:file "replication" :depends-on ("environment"))
;;                          (:file "sequence" :depends-on ("environment"))
;;                          (:file "transaction" :depends-on ("environment"))
                         (:file "translators"
                                :depends-on ("database" "cursor" "environment" "logging")))))
  :in-order-to ((test-op (load-op berkeley-db-test)))
  :perform (test-op :after (op c)
                    (describe (funcall (intern "RUN-TESTS" :lift) 
				       :suite (intern "TEST-BERKELEY-DB"
						      :berkeley-db-test)))))

(defmethod operation-done-p ((o test-op) (c (eql (find-system :berkeley-db))))
  (values nil))

(defsystem berkeley-db-test
  :description "This is a translation of the TCL tests included
  with the Berkeley DB source distribution."
  :depends-on (berkeley-db lift)
  :serial t
  :components ((:module "test"
			:components ((:file "environment")
;; 				     (:file "archive")
;; 				     (:file "file-operations")
;; 				     (:file "locking")
;; 				     (:file "logging")
;; 				     (:file "memory-pool")
;; 				     (:file "transaction")
;; 				     (:file "deadlock-detection")
;; 				     (:file "subdatabase")
;; 				     (:file "byte-order")
;; 				     (:file "recno-backing-file")
;; 				     (:file "dbm-interface")
;; 				     (:file "ndbm-interface")
;; 				     (:file "hsearch-interface")
;; 				     (:file "secondary-index")
;; 				     (:file "replication")
;; 				     (:file "test")
				     ))))