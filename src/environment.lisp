(in-package :bdb)

;; (defmacro with-env ((var env home &key flags mode) &body body)
;;   `(let ((,var ,env))
;;      (prog1
;; 	 (progn
;; 	   (open-env ,var ,home
;; 		     ,@(if flags `(:flags ,flags))
;; 		     ,@(if mode `(:mode ,mode)))
;; 	   ,@body)
;;        (close-env ,var))))

;; (defmacro with-envs (envs &body body)
;;   (if (null envs)
;;       (values body)
;;       `(with-env ,(car envs)
;; 	 ,(with-envs (cdr envs) (values body)))))

(defclass environment (bdb-mixin)
  ())

(defslot environment recovery-function :pointer :writer
	 "May not be called after OPEN."
         :int environment thing bdb-types:log-sequence-number
	 bdb-types:recovery-operation)
(defslot environment data-directory path :accessor)
(defslot environment alive?-function :pointer :writer
	 ""
	 boolean environment :pid bdb-types:thread-id)
;; lock-conflicts
(defslot environment lock-reject-method bdb-types:reject-rule :accessor)
(defslot environment max-lockers :uint32 :accessor)
(defslot environment max-locks :uint32 :accessor)
(defslot environment max-locked-objects :uint32 :accessor)

(defcfun ("db_env_create" %make-environment) status
  (dbenvp :pointer)
  (flags flags))

(defun make-environment (&key flags)
  (with-foreign-object (env :pointer)
    (%make-environment env flags)
    (convert-from-foreign (mem-ref env :pointer) 'environment)))

(defmethod close ((env environment) &key flags)
  (make-call env 'bdb-types:close flags flags))

(defmethod remove-database ((env environment) file &key database txn flags)
  (make-call env 'bdb-types:remove-database
	     transaction txn
	     path file
	     :string database
	     flags flags))

(defmethod rename-database ((env environment) file database new-name
			    &key txn flags)
  (make-call env 'bdb-types:rename-database
	     transaction txn
	     path file
	     :string database
	     :string new-name
	     flags flags))

(defmethod error ((env environment) message &optional error)
  (if error ; FIXME: these actually return void, dunno if it matters
      (make-call env 'bdb-types:error :int error :string message)
      (make-call env 'bdb-types:short-error :string message)))

(defmethod clean-failed-threads ((env environment) &key flags)
  (make-call env 'bdb-types:clean-failed-threads flags flags))

(defmethod open ((env environment) db-home
		 &key flags (mode 0) &allow-other-keys)
  (make-call env 'bdb-types:open
	     path db-home
	     flags flags
	     :int mode))

(defmethod remove ((env environment) home &key database flags)
  (declare (ignore database))
  (make-call env 'bdb-types:remove path home flags flags))
