(in-package :bdb)

(defmacro with-open-database ((var file &key env txn (database (null-pointer))
				   type (mode 0) 
				   make-flags open-flags close-flags)
			      &body body)
  `(let ((,var (make-database :env ,env :flags ,make-flags)))
     (unwind-protect
	  (progn
	    (open ,var ,file :txn ,txn :database ,database
		  :type ,type :flags ,open-flags :mode ,mode)
	    ,@body)
       (if ,var
	   (close ,var :flags ,close-flags)
	   (foreign-free (convert-to-foreign ,var 'database))))))

;; (defmacro with-open-databases ((&rest databases) &body body)
;;   (if databases
;;       `(with-open-database ,(car databases)
;; 	 ,(with-open-databases (cdr databases)
;; 	    body))
;;       `,@body))

(defclass database (bdb-mixin)
  ())

(defslot database append-recno-function :pointer :writer
	 "May not be called after OPEN."
	 :int database thing bdb-types:recno)
(defslot database byte-order :int :accessor
	 "The setf method may not be called after OPEN.")
(defslot database byteswapped? boolean :reader)
(defslot database duplicate-comparison-function :pointer :writer
	 "May not be called after OPEN."
	 :int database thing thing)
(defslot database environment environment :reader)
(defslot database file-descriptor :int :reader)
;;(defslot database memory-pool-file 'memory-pool-file :reader)
(defslot database page-size :uint32 :accessor
	 "The setf method may not be called after OPEN.")
(defslot database panic-function :pointer :writer
	 ""
	 :void environment :int)
(defslot database transactional? boolean :reader)
(defslot database type bdb-types:database-type :reader)

;; ;;; Need fopen because set-errfile requires a filehandle
;; (uffi:def-function "fopen" ((filename :cstring) (mode :cstring))
;;   :returning :pointer-void)
;; (defun f-open (filename mode)
;;   (uffi:with-cstrings ((c-file filename) (c-mode mode))
;;     (fopen c-file c-mode))) 

;; (defun set-db-errfile (db errfile)
;;   (handle-result (set-errfile db (f-open errfile "a"))))

(defcfun ("db_create" %make-database) status
  (dbp :pointer)
  (dbenv environment)
  (flags flags))

(defun make-database (&key env flags)
  (let ((db (foreign-alloc :pointer)))
    (%make-database db env flags)
    (convert-from-foreign (mem-ref db :pointer) 'database)))

(defun associate-index (primary-db secondary-db callback &key txn flags)
  (make-call primary-db 'bdb-types:associate-index
	     transaction txn
	     database secondary-db
	     :pointer callback
	     flags flags))

(defmethod close ((db database) &key flags)
  (make-call db 'bdb-types:close flags flags))

(defmethod compact ((db database) &key txn start stop compact-config flags end)
  (make-call db 'bdb-types:compact
	     transaction txn
	     thing start
	     thing stop
	     :pointer (null-pointer) ; FIXME: need to use this param
	     flags flags
	     thing end))

(defmethod get-cursor ((db database) &key txn flags)
  (with-foreign-object (cursor :pointer)
    (make-call db 'bdb-types:get-cursor
	       transaction txn
	       :pointer cursor
	       flags flags)
    (convert-from-foreign (mem-ref cursor :pointer) 'cursor)))

(defmethod delete-record ((db database) key &key txn flags)
  (make-call db 'bdb-types:delete-record thing key transaction txn flags flags))

(defmethod error ((db database) message &optional error)
  (if error ; FIXME: these actually return void, dunno if it matters
      (make-call db 'bdb-types:error :int error :string message)
      (make-call db 'bdb-types:short-error :string message)))

(defmethod get-record ((db database) key &key txn flags)
  (with-foreign-object (data 'thing)
    (make-call db 'bdb-types:get-record
	       transaction txn
	       thing key
	       :pointer data
	       flags flags)
    (convert-from-foreign data 'thing)))

(defmethod get-record-and-primary-key ((db database) key &key txn flags)
  (with-foreign-objects ((pkey 'thing) (data 'thing))
    (make-call db 'bdb-types:get-record
	       transaction txn
	       thing key
	       :pointer pkey
	       :pointer data
	       flags flags)
    (values (convert-from-foreign data 'thing)
	    (convert-from-foreign pkey 'thing))))

(defmethod join ((primary-db database) cursors &key flags)
  (with-foreign-object (join-cursor :pointer)
    (make-call primary-db 'bdb-types:join
	       cursor-list cursors
	       :pointer join-cursor
	       flags flags)
    (convert-from-foreign (mem-ref join-cursor :pointer) 'join-cursor)))

(defun key-range (db key &key txn flags)
  (with-foreign-object (key-range 'key-range)
    (make-call db 'bdb-types:key-range
	       transaction txn
	       thing key
	       :pointer key-range
	       flags flags)
    (convert-from-foreign key-range 'key-range)))

(defmethod open ((db database) file
		 &key txn (database (null-pointer)) (type :unknown)
		 flags (mode 0) &allow-other-keys)
  (make-call db 'bdb-types:open
	     transaction txn
	     path file
	     :string database
	     bdb-types:database-type type
	     flags flags
	     :int mode))

(defmethod put-record ((db database) key data &key txn flags)
  (make-call db 'bdb-types:put-record
	     transaction txn
	     thing key
	     thing data
	     flags flags))

(defmethod remove ((db database) file &key (database (null-pointer)) flags)
  (make-call db 'bdb-types:remove path file :string database flags flags))

(defmethod rename (db file database new-name &key flags)
  (make-call db 'bdb-types:rename
	     path file
	     :string database
	     :string new-name
	     flags flags))

(defmethod print-statistics ((db database) &key flags)
  (make-call db 'bdb-types:print-statistics flags flags))

(defmethod sync ((db database) &key flags)
  (make-call db 'bdb-types:sync flags flags))

(defmethod upgrade ((db database) file &key flags)
  (make-call db 'bdb-types:upgrade path file flags flags))

(defmethod verify ((db database) file &key database output-file flags)
  (make-call db 'bdb-types:verify
	     path file
	     :string database
	     :pointer (null-pointer)
	     flags flags))

(defclass unordered-database (database)
  ())

(defclass btree-database (unordered-database)
  ())

(defslot btree-database comparison-function :pointer :writer
	 "May not be called after OPEN."
	 :int database thing thing)
(defslot btree-database minkey :uint32 :accessor
	 "The setf method may not be called after OPEN.")
(defslot btree-database prefix-function :pointer :writer
	 "May not be called after OPEN."
	 :size database thing thing)

(defmethod  statistics ((db btree-database) &key txn flags)
  (with-foreign-object (stat 'bdb-types:btree-statistics)
    (make-call db 'bdb-types:statistics transaction txn flags flags)
    (mem-ref stat 'bdb-types:btree-statistics)))

(defclass hash-database (unordered-database)
  ())

(defslot hash-database fill-factor :uint32 :accessor
	 "The setf method may not be called after OPEN.")
(defslot hash-database num-elements :uint32 :accessor
	 "The setf method may not be called after OPEN.")

(defmethod (setf hash-function) (function (db hash-database))
  "May not be called after OPEN."
  (make-call db 'bdb-types:set-hash-function
             :pointer (get-callback
                       (make-callback
                        (lambda (db bytes length)
                          (funcall function db (make-array length
                                                           :element-type '(unsigned-byte 8)
                                                           :initial-contents (dotimes (index length (reverse byte-list))
                                                                               (push (mem-aref bytes :char index) byte-list)))))
                        :uint32 database :pointer :uint32))))

(defmethod  statistics ((db hash-database) &key txn flags)
  (with-foreign-object (stat 'bdb-types:hash-statistics)
    (make-call db 'bdb-types:statistics transaction txn flags flags)
    (mem-ref stat 'bdb-types:hash-statistics)))

(defclass ordered-database (database)
  ())

(defslot ordered-database record-length :uint32 :accessor)
(defslot ordered-database padding-character :int :accessor)

(defclass queue-database (ordered-database)
  ())

(defslot queue-database extent-size :uint32 :accessor)

(defmethod  statistics ((db queue-database) &key txn flags)
  (with-foreign-object (stat 'bdb-types:queue-statistics)
    (make-call db 'bdb-types:statistics transaction txn flags flags)
    (mem-ref stat 'bdb-types:queue-statistics)))

(defclass recno-database (ordered-database)
  ())

(defslot recno-database delimiting-byte :int :accessor)
(defslot recno-database source-file path :accessor)

(defmethod  statistics ((db recno-database) &key txn flags)
  "Recno uses the same statistics structure as btree."
  (with-foreign-object (stat 'bdb-types:btree-statistics)
    (make-call db 'bdb-types:statistics transaction txn flags flags)
    (mem-ref stat 'bdb-types:btree-statistics)))

