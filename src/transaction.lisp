(in-package bdb)

(defmacro txn-call (field args)
  `(bdb-call-nil txn 'db-txn ,field ,args))

(defun abort-txn (txn)
  (txn-call 'abort ()))

(defun commit-txn (txn &key flags)
  (txn-call 'commit (((flags->int flags) :unsigned-int))))

(defun discard-txn (txn &key flags)
  (txn-call 'discard (((flags->int flags) :unsigned-int))))

(defun txn-id (txn)
  (bdb-call txn 'db-txn 'id () :unsigned-int))

;; (defun prepare-txn (txn gid)
;;   (txn-call 'prepare (gid (* :unsigned-byte))))

(defun set-txn-timeout (txn timeout flags)
  (txn-call 'set-timeout ((timeout :unsigned-int)
			  ((flags->int flags) :unsigned-int))))