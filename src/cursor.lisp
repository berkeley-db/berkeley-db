(in-package bdb)

(defmacro with-cursor ((name db &key txn flags) &body body)
  `(let ((,name ,(db-cursor db
			    (if txn `(:txn ,txn))
			    (if flags `(:flags ,flags)))))
     (prog1
	 (progn
	   ,@body)
       (close-cursor ,name))))

;; (defmacro with-cursors (cursors &body body)
;;   (if (null cursors)
;;       (values body)
;;       `(with-cursor ,(car cursors)
;; 	 ,(with-cursors (cdr cursors) body))))

(defclass join-cursor (pointer-wrapper)
  ()
  (:documentation "This type of cursor is only created during a
  database join, and is restricted to only using
  GET-RECORD[-AND-PRIMARY-KEY] and CLOSE."))

(defmethod close ((cursor join-cursor) &key flags)
  (declare (ignore flags))
  (make-call cursor 'bdb-types:close))

(defmethod get-record ((cursor join-cursor) key &key txn flags)
  (declare (ignore txn))
  (with-foreign-object (data 'thing)
    (make-call cursor 'bdb-types:get-record
	       thing key
	       :pointer data
	       flags flags)
    (convert-from-foreign data 'thing)))

(defmethod get-record-and-primary-key ((cursor join-cursor) key &key txn flags)
  (declare (ignore txn))
  (with-foreign-objects ((pkey 'thing) (data 'thing))
    (make-call cursor 'bdb-types:get-record
	       thing key
	       :pointer pkey
	       :pointer data
	       flags flags)
    (values (convert-from-foreign data 'thing)
	    (convert-from-foreign pkey 'thing))))

(defclass cursor (join-cursor)
  ())

(defmethod count-duplicates ((cursor cursor) &key flags)
  (with-foreign-object (count 'bdb-types:recno)
    (make-call count 'bdb-types:count-duplicates :pointer count flags flags)
    (mem-ref count 'bdb-types:recno)))

(defmethod delete-record ((cursor cursor) key &key txn flags)
  (declare (ignore txn))
  (make-call cursor 'bdb-types:delete-record thing key flags flags))

(defmethod duplicate ((cursor cursor) &key flags)
  (with-foreign-object (dup :pointer)
    (make-call cursor 'bdb-types:duplicate :pointer dup flags flags)
    (convert-from-foreign (mem-ref dup :pointer) 'cursor)))

(defmethod put-record ((cursor cursor) key data &key txn flags)
  (declare (ignore txn))
  (make-call cursor 'bdb-types:put-record
	     transaction txn
	     thing key
	     thing data
	     flags flags))
