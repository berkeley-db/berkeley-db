(in-package :bdb)

(defvar GiB (expt 1024 3))

(defctype boolean :int)
(defctype path :string)
(defctype path-list :pointer)
(defctype status :int)

(define-condition bdb-error (cl:error)
  ((error-code :initarg :code :reader error-code))
  (:report (lambda (condition stream)
	     (format stream "~d: ~a"
		     (error-code condition)
		     (error-string (error-code condition))))))

(defctype database bdb-types:database)
(defctype thing bdb-types:thing)
(defctype cursor bdb-types:cursor)
(defctype join-cursor bdb-types:cursor)
(defctype cursor-list :pointer)
(defctype key-range bdb-types:key-range)
(defctype environment bdb-types:environment)
(defctype transaction bdb-types:transaction)
(defctype flags bdb-types:flags)

(defclass pointer-wrapper ()
  ((pointer :initarg :pointer :reader pointer)))

(defclass transaction (pointer-wrapper)
  ())

(defmacro make-call (obj slot &rest args)
  (let ((object (gensym "OBJ")))
    `(let ((,object ,obj))
       (foreign-funcall (foreign-slot-value (pointer ,object)
					    (class-name (class-of ,object))
					    ,slot)
			:pointer (pointer ,object)
			,@args
			status)
       (values))))

(defmacro make-callback (function return-type &rest parameter-types)
  (let ((parameter-names (mapcar (lambda (x)
				   (declare (ignore x))
				   (gensym "CALLBACK-ARG"))
                                 parameter-types)))
    `(defcallback ,(intern (string (gensym "CALLBACK")) *package*) ,return-type
         ,(mapcar #'list parameter-names parameter-types)
       (funcall ,function ,@parameter-names))))

(defmacro defslot (type slot-name slot-type access &optional (documentation "")
		   &rest types)
  `(progn
     ,(if (member access '(:reader :accessor))
          `(defmethod ,slot-name ((object ,type))
             ,documentation
             (with-foreign-object (value ',slot-type)
               (make-call object
			  ',(intern (format nil "GET-~a" slot-name) :bdb-types)
			  :pointer value)
               (mem-ref value ',slot-type))))
     ,(if (member access '(:writer :accessor))
          `(defmethod (setf ,slot-name) (value (object ,type))
             ,documentation
             (make-call object
			',(intern (format nil "SET-~a" slot-name) :bdb-types)
                        ,slot-type ,(if (eq slot-type :pointer)
					`(get-callback (make-callback value
								      ,@types))
					'value))))))

(defclass bdb-mixin (pointer-wrapper)
  ())

(defslot bdb-mixin encryption-flags flags :reader)
(defslot bdb-mixin error-function :pointer :writer
	 ""
	 :void environment :string :string)
(defslot bdb-mixin message-function :pointer :writer
	 ""
	 :void environment :string)
;; error-file, message-file
(defslot bdb-mixin error-prefix :string :accessor)
(defslot bdb-mixin feedback-function :pointer :writer
	 ""
	 :void database bdb-types:opcode :int)
(defslot bdb-mixin flags flags :accessor
	 "The setf method may not be called after OPEN.")

;; (defmethod (setf allocation-functions) (functions (object bdb-mixin))
;;   "May not be called after OPEN.")

(defmethod cache-size ((object bdb-mixin))
  (with-foreign-objects ((gbytes :uint32) (bytes :uint32) (ncache :int))
    (make-call object 'bdb-types:get-cache-size
               :pointer gbytes
               :pointer bytes
               :pointer ncache)
    (list (+ (* (mem-ref gbytes :uint32) GiB) (mem-ref bytes :uint32))
          (mem-ref ncache :int))))

(defmethod (setf cache-size) (cache-info (object bdb-mixin))
  "May not be called after OPEN, or on a database opened within an environment."
  (let ((bytes (if (listp cache-info) (first cache-info) cache-info))
        (num-caches (if (listp cache-info) (or (second cache-info) 1) 1)))
    (make-call object 'bdb-types:set-cache-size
               :uint32 (cl:truncate (/ bytes GiB))
               :uint32 (mod bytes GiB)
               :int num-caches)))

(defmethod (setf encryption) (encryption-info (object bdb-mixin))
  "May not be called after OPEN."
  (let ((password (if (listp encryption-info)
                      (first encryption-info)
                      encryption-info))
        (flags (if (listp encryption-info) (second encryption-info))))
    (make-call object 'bdb-types:set-encryption
               :string password
               flags flags)))
