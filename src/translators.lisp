(in-package :bdb)

(defmethod translate-to-foreign (value (name (eql 'boolean)))
  (if value 1 0))

(defmethod translate-from-foreign (value (name (eql 'boolean)))
  (= value 1))

(defmethod translate-to-foreign ((value null) (name (eql 'path)))
  (null-pointer))

(defmethod translate-to-foreign ((value pathname) (name (eql 'path)))
  (convert-to-foreign (namestring (truename value)) :string))

(defmethod translate-to-foreign ((value string) (name (eql 'path)))
  (convert-to-foreign value :string))

(defmethod translate-from-foreign (value (name (eql 'path)))
  (parse-namestring value))

(defmethod translate-from-foreign (value (name (eql 'path-list)))
  (loop for i from 0 and string = (mem-aref value :string i)
     until (null-pointer-p string)
     collect string))

(defmethod translate-from-foreign (value (name (eql 'status)))
  (if (/= value 0) (cl:error 'bdb-error :code value) (values)))

(defmethod translate-to-foreign (value (name (eql 'database)))
  (if value (pointer value) (null-pointer)))

(defmethod translate-to-foreign (value (name (eql 'cursor)))
  (if value (pointer value) (null-pointer)))

(defmethod translate-to-foreign (value (name (eql 'cursor-list)))
  (foreign-alloc 'cursor :initial-contents value :null-terminated-p t))

(defmethod translate-to-foreign (value (name (eql 'environment)))
  (if value (pointer value) (null-pointer)))

(defmethod translate-to-foreign (value (name (eql 'transaction)))
  (if value (pointer value) (null-pointer)))

(defmethod translate-from-foreign (value (name (eql 'database)))
  (unless (null-pointer-p value) (make-instance 'database :pointer value)))

(defmethod translate-from-foreign (value (name (eql 'cursor)))
  (unless (null-pointer-p value) (make-instance 'cursor :pointer value)))

(defmethod translate-from-foreign (value (name (eql 'environment)))
  (unless (null-pointer-p value) (make-instance 'environment :pointer value)))

(defmethod translate-from-foreign (value (name (eql 'transaction)))
  (unless (null-pointer-p value) (make-instance 'transaction :pointer value)))

(defmethod translate-from-foreign (value (name (eql 'key-range)))
  (list (foreign-slot-value value 'key-range 'bdb-types:less)
	(foreign-slot-value value 'key-range 'bdb-types:equal)
	(foreign-slot-value value 'key-range 'bdb-types:greater)))

(defmethod translate-to-foreign ((value string) (name (eql 'thing)))
  (let ((dbt (foreign-alloc 'thing)))
    (setf (foreign-slot-value dbt 'thing 'bdb-types:size) (1+ (length value)))
    (setf (foreign-slot-value dbt 'thing 'bdb-types:data)
	  (convert-to-foreign value :string))
    dbt))

(defmethod translate-from-foreign (value (name (eql 'thing)))
  (convert-from-foreign (foreign-slot-value value 'thing 'bdb-types:data)
			:string))

(defmethod translate-to-foreign ((value list) (name (eql 'flags)))
  (apply #'+ (mapcar (lambda (flag) (convert-to-foreign flag name)) value)))
