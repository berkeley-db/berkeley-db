(defpackage bdb-types
  (:documentation "This package prevents us from accidentally
  exporting the groveled types."))

(defpackage berkeley-db
  (:nicknames #:bdb)
  (:documentation
"Berkeley DB support for Common Lisp with CFFI

This interface is basically identical to the C one, except:

* underscores are translated to hyphens
* error codes are thrown, not returned
* out-params are returned, not passed
")
  (:shadow #:abort #:close #:count #:error #:open #:remove #:truncate)
  (:use #:cl #:cffi)
  (:export #:with-open-database #:with-open-environment

	   ;; multiple objects
	   #:allocation-functions #:cache-size #:close #:encryption-flags #:encryption #:error #:error-file #:error-function #:error-prefix #:feedback-function #:flags #:message-file #:message-function #:name #:open #:print-statistics #:remove #:statistics

	   ;; cursors
	   #:count #:duplicate

	   ;; databases
	   #:append-recno-function #:associate-index #:byte-order #:byteswapped? #:compact #:comparison-function #:delimiting-byte #:duplicate-comparison-function #:environment #:extent-size #:fill-factor #:hash-function #:make-database #:join #:key-range #:memory-pool-file #:minkey #:num-elements #:padding-character #:page-size #:panic-function #:prefix-function #:record-length #:remove-database #:rename #:rename-database #:source-file #:sync #:transactional? #:truncate #:type #:upgrade #:verify

	   ;; environments
	   #:make-environment

	   ;; locks
	   #:acquire-lock #:free-lock-id #:lock-id
	   #:lock-statistics #:print-lock-statistics
	   #:release-lock

	   ;; records
	   #:delete-record #:get-record #:get-record-and-primary-key
	   #:put-record
 
	   ;; transactions
	   #:abort #:begin-transaction #:checkpoint-transaction #:commit
	   #:discard #:id #:prepare #:print-transaction-statistics
	   #:recover-transactions #:transaction-statistics

 	   #:version))
