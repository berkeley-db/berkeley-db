(in-package :bdb)

(defslot environment log-buffer-size :uint32 :accessor)
(defslot environment log-directory path :accessor)
(defslot environment log-file-mode :int :accessor)
(defslot environment log-file-max-size :uint32 :accessor)
(defslot environment log-region-max-size :uint32 :accessor)

(defmethod log-archive ((env environment) &key flags)
  (with-foreign-object (list 'path-list)
    (make-call env 'bdb-types:get-log-archive :pointer list flags flags)
    (mem-ref list 'path-list)))

(defmethod log-file ((env environment) lsn)
  (let ((length 20)) ; should be long enough
    (with-foreign-object (name :char length)
      (make-call env 'bdb-types:get-log-file
		 bdb-types:log-sequence-number lsn
		 :string name
		 :size length)
      name)))

(defmethod flush-log ((env environment) &key max-lsn)
  (make-call env 'bdb-types:flush-log bdb-types:log-sequence-number max-lsn))

(defmethod print-to-log ((env environment) message &key txn)
  (make-call env 'bdb-types:print-to-log transaction txn :string message))

(defmethod put-log-record ((env environment) lsn data &key flags)
  (make-call env 'bdb-types:put-log-record
	     bdb-types:log-sequence-number lsn
	     thing data
	     flags flags))

(defmethod log-statistics ((env environment) &key flags)
  (with-foreign-object (stats 'bdb-types:log-statistics)
    (make-call env 'bdb-types:get-log-statistics :pointer stats flags flags)
    (mem-ref stats 'bdb-types:log-statistics)))

(defmethod print-log-statistics ((env environment) &key flags)
  (make-call env 'bdb-types:print-log-statistics flags flags))

(defclass log-cursor (pointer-wrapper)
  ())

(defmethod get-log-cursor ((env environment) &key flags)
  (with-foreign-object (cursor 'bdb-types:log-cursor)
    (make-call env 'bdb-types:get-log-cursor :pointer cursor flags flags)
    (mem-ref cursor 'bdb-types:log-cursor)))

(defmethod close ((cursor log-cursor) &key flags)
  (make-call cursor 'bdb-types:close flags flags))

(defmethod get-record ((cursor log-cursor) lsn &key txn flags)
  (declare (ignore txn))
  (make-call cursor 'bdb-types:get-record
	     bdb-types:log-sequence-number lsn
	     flags flags))
