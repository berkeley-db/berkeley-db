(defpackage berkeley-db-test
  (:use #:cl #:bdb #:lift)
  (:shadowing-import-from #:bdb #:abort #:close #:count #:error #:open #:remove
			  #:truncate))

(in-package :berkeley-db-test)

(defvar rpc_server "localhost")
(defvar rpc_path #p"./")
(defvar rpc_testdir (merge-pathnames #p"TESTDIR" rpc_path))

;; set src_root @srcdir@/..
;; set test_path @srcdir@/../test
;; set je_root @srcdir@/../../je

(defvar testdir #p"./TESTDIR/")
(ensure-directories-exist testdir)

(deftestsuite test-berkeley-db ()
  ((testfile (merge-pathnames #p"env.db" testdir))
   (t1 (merge-pathnames #p"t1" testdir))))

(deftestsuite test-environment (test-berkeley-db)
  ((env (make-environment))))

(addtest remove-environment
  ;; OPEN without :create should error
  (ensure-error (open env testdir))

  ;; OPEN with create
  (open env testdir :flags '(:create) :mode 0644)
  ;; make sure that CLOSE works
  (close env)
  ;; make sure we can reOPEN
  (open env testdir)
  ;; remove environment
  (close env)
  (remove env testdir :flags '(:force))
  ;; REMOVE on open environment, without :FORCE
  (open env testdir :flags '(:create) :mode 0644)
  (ensure-error (remove env testdir))
  (close env)
  ;; REMOVE on open environment, with :FORCE
  (open env testdir :flags '(:create) :mode 0644)
  (remove env testdir :flags '(:force))
  (close env)


;;   (open env testdir :flags '(:create) :mode 0644)
;;   (handler-case (progn (remove env testdir :flags '(:force)) (fail))
;;     (bdb-error (err) (eq (code err) :no-such-file)))
  )